chrome.webRequest.onBeforeSendHeaders.addListener(
    function (details) {
        console.log(details)
        //details holds all request information. 
        /* for (var i = 0; i < details.requestHeaders.length; ++i) {
            //Find and change the particular header.
            if (details.requestHeaders[i].name === 'Origin') {
                details.requestHeaders[i].value = "https://example.com";
                break;
            }
        }

        return { requestHeaders: details.requestHeaders }; */
    },
    { urls: ["<all_urls>"] },
	// 額外的 request-header 訊息，若沒有添加，就只有基本訊息
	// 其中，
	// 		blocking，使請求同步，以便您可以修改請求標頭
	//		requestHeaders，提供額外的 request 資訊
    ['blocking', "requestHeaders"]  
);