console.log("hello from popup-script")

var btn = document.getElementById("send");

// 按下popup.html 中的按鈕後才發送訊息
btn.addEventListener("click", function (e) {
    console.log("button clicked ...")

    // ==== step1，透過 chrome.tabs.query() 取得當前瀏覽頁面，並將結果傳遞給回調 ====
    chrome.tabs.query({ "active": true }, function (tabs) {

        // ==== step2，傳遞訊息給 contesnt-script ====
        // 需要傳遞進來的 tab 取得 tabId ，用來指定是哪一個分頁中的 contesnt-script
        // chrome.tabs.sendMessage( tabId, 要傳送出去的訊息, 處理傳遞回來的訊息的回調函數 )
        chrome.tabs.sendMessage(tabs[0].id, "Test message X", (responseObject) => {
            console.log(`Message '${responseObject.message}' from Sender '${responseObject.sender}' `)
        });
    });
});
