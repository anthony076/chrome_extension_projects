
console.log("hello from bg.js");

// 後端要回復的訊息
var responseObject = {
    message: "HELLO WEBPAGE, I AM BG",
    sender: "bs.js"
};

// onMessageExternal，接收從插件以外傳遞的訊息，例如從網頁中
chrome.runtime.onMessageExternal.addListener(
    function (message, sender, sendResponse) {
        sendResponse(responseObject);
        console.log("Message '" + message + "' from Sender '" + sender.url + "'");
    }
);
