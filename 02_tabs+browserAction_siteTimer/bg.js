

// 分頁建立並開啟時，觸發的回調函數
chrome.tabs.onCreated.addListener(function (tab) {
    console.log("onCreated ...")
    checkUrl(tab.url);
});

// 分頁切換時，觸發的回調函數
chrome.tabs.onActivated.addListener(function (activeInfo) {
    console.log("onActivated ...")
    chrome.tabs.get(activeInfo.tabId, function (tab) {
        checkUrl(tab.url);
    });
});

// 分頁reload 或 輸入新網址 時，觸發的回調函數
chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
    console.log("onUpdated ...")
    if (changeInfo.url !== undefined) {
        checkUrl(changeInfo.url);
    }
});

var count = 30; //計時器初始值
var time = 30;  //預設響鈴時間
var myTimer;    //用來更新計時器的函數

// 檢查當前網址是否是目標網站
function checkUrl(url) {
    var regex = new RegExp('^https://www.facebook.com/');

    // 若是目標網站，
    if (regex.test(url)) {
        //若計時器已經存在，清除舊的計時器
        if (myTimer !== undefined) {
            clearTimeout(myTimer);
        }
        // 重設計時器
        count = time;

        // 開始計時
        myTimer = countDown();
    }
    else {
        // 若不是目標網站，清除計時器
        if (myTimer !== undefined) {
            clearTimeout(myTimer);
        }
    }
}

// 執行計時的函數
function countDown() {
    count--;

    // 更新插件標記上顯示的數值
    chrome.browserAction.setBadgeText({ text: count.toString() });

    // 計時中
    if (count > 0) {
        console.log("Timer:" + myTimer)
        myTimer = setTimeout(countDown, 1000);
    }
    // 計時完畢
    else {
        alert('Time Up ...');

        // 重設計時器
        count = time;
        myTimer = setTimeout(countDown, 1000);
    }
    chrome.browserAction.setBadgeText({ text: count.toString() });

}