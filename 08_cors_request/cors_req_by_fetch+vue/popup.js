const vm = new Vue({
    el: "#app",
    data: {
        item: { County: undefined, UVI: 0 }
    },
    methods: {
        getAir: function () {
            var url = "http://opendata.epa.gov.tw/ws/Data/UV/?$orderby=PublishAgency&$skip=0&$top=1&format=json";

            fetch(url)
                .then(function (res) {
                    return res.json();
                })
                .then(function (jdata) {
                    vm.item = jdata[0]
                });
        }
    },
    mounted() {
        this.getAir();
    },
})















