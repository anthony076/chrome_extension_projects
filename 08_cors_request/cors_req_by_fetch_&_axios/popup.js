
var url = "http://opendata.epa.gov.tw/ws/Data/UV/?$orderby=PublishAgency&$skip=0&$top=1&format=json";

document.getElementById('fetch').addEventListener('click', function () {
    getFromFetch()
}, false);

document.getElementById('axios').addEventListener('click', function () {
    getFromAxios()
}, false);


getFromFetch = () => {
    fetch(url)
        .then(function (res) {
            return res.json();
        })
        .then(function (jdata) {
            document.getElementById("msg").innerText = "Fetch: " + jdata[0].UVI
        });
}

getFromAxios = () => {
    axios.get(url)
        .then(function (response) {
            console.log(response)
            document.getElementById("msg2").innerText = "Axios: " + response.data[0].UVI
        })
        .catch(function (error) {
            console.log(error)
        });
}
















