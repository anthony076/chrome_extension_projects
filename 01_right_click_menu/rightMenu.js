
// ==== step1. 建立顯示在右件選單中的選項 ====
// 注意，此範例中，若沒有選擇文字，就不會出現此選單選項
var OpenGoogle = {
    "id": "OpenGoogle",          // 選單選項的 id
    "title": "Translate: %s",
    "contexts": ["selection"],            // 此選項是在文字被選取後才會出現在選單中
};

// ==== step2，註冊選單 ====
// 多個選單選項，會自動排列成階層

// 建立選單選項1
chrome.contextMenus.create(OpenGoogle);

// 建立選單選項2
chrome.contextMenus.create({
    "id": "clickme",
    "title": "Click Me",
    "contexts": ["all"]
});

// ==== step3，選單中的事件被觸發後的回調函數 ====
chrome.contextMenus.onClicked.addListener(function (clickData) {
    console.log(clickData)

    if (clickData.menuItemId == "OpenGoogle") {
        var selection = clickData.selectionText;
        chrome.tabs.create({ url: "https://translate.google.com.tw/?iop=translate&sl=en&tl=zh-TW&text=" + selection });
    }

    if (clickData.menuItemId == "clickme") {
        alert("You click me")
    }
});