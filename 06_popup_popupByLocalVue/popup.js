
let vm = new Vue({
    el: "#app",
    data: {
        welcom: "hello world",
        toggle: false
    },
    methods: {
        change_name: () => {
            vm.toggle = !vm.toggle

            if (vm.toggle) {
                vm.welcom = "hello ching"
            } else {
                vm.welcom = "hello wold"
            }

        }
    }
})
