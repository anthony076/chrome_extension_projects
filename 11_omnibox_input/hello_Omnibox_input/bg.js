
// ==== 使用者點擊(觸發)多功能輸入框後被觸發 ====
chrome.omnibox.onInputStarted.addListener(function () {
    console.log("<InputStarted>");
});

// ==== 多功能輸入框的輸入改變時觸發 ====
//預設值建議值
var suggestResultOne = {
    "content": "Some content",
    "description": "Description"
};

var suggestResults = [suggestResultOne];

// 自定義建議值
var descriptions = {
    "a": ["actions", "alarms", "apps",],
    "b": ["background-page", "bookmarks", "browser-action"],
    "c": ["commands", "content-script", "context-menu"],
    "d": ["dashboard", "declarativeContent"],
    "e": ["event-script", "examples"],
    "h": ["history"],
    "i": ["incognito", "inject"],
    "m": ["management page", "manifest", "match pattern", "messaging"],
    "o": ["omnibox"],
    "p": ["page-action", "permissions", "plugins", "popup"],
    "r": ["resources panel", "runtime"],
    "s": ["sources panel", "store", "storage"],
    "t": ["tabs", "themes"]
};

//當使用者輸入的字符合上列時，將所有的建議清單裝成一個陣列回傳
//如果沒有符合就回傳預設值
function getSuggestResults(key) {
    var results = [];
    if (!descriptions[key] || !key) return suggestResults;

    for (var i = 0; i < descriptions[key].length; i++) {
        var suggestResult = {};
        suggestResult["content"] = descriptions[key][i];
        suggestResult["description"] = "Search '" + descriptions[key][i] + "' on developer.chrome.com";
        results[i] = suggestResult;
    }
    return results;
}

chrome.omnibox.onInputChanged.addListener(function (text, suggest) {
    console.log("<InputChanged> Text: " + text);
    suggest(getSuggestResults(text))
});

// ==== 使用者在多功能輸入框中按下 Enter 鍵後觸發 ====
// 按下 Enter 鍵後，顯示建議選單

var searchUrl = "https://www.google.com/";
searchUrl += "search?q=chrome+extensions+developers+";

chrome.omnibox.onInputEntered.addListener(function (text, disposition) {
    console.log("<InputEntered> Text: " + text);
    chrome.tabs.create({ "url": searchUrl + text });
});
