import sys,json, struct

def send_message(MSG_DICT):
	# dict to json
	msg_json = json.dumps(MSG_DICT, separators=(",", ":"))

	# json to string
	msg_json_utf8 = msg_json.encode("utf-8")

	# ==== json to byte code ====
	# 跟讀取方式一樣，先寫入訊息長度
	sys.stdout.buffer.write(struct.pack("i", len(msg_json_utf8)))
	# 再寫入訊息內容
	sys.stdout.buffer.write(msg_json_utf8)

def read_message():
	f = open("log.txt", "a+")

    # ==== step1，取得 message 的長度 ====
    # chrome 傳遞進來的訊息格式: 訊息長度(佔4bytes) + 訊息內容
	length_bytes = sys.stdin.buffer.read(4)			# read binary-format
	message_length = struct.unpack("i", length_bytes)[0]	# binary -> int

	# ==== step2，讀取 message 內容 ====
	# 將 byte-format 的 message 轉換成 utf-8 string
	message_bytes = sys.stdin.buffer.read(message_length)
	message = message_bytes.decode("utf-8")

	f.write(f"[message_length]:{message_length}\n")
	f.write(f"[message]:{message}\n")
	f.close()

test = {"name": "response1", "text": "Hello, extension."}
read_message()
send_message(test)