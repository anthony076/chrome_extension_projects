function updateResult(obj, state) {
    document.getElementById(obj).innerHTML = state;
}
function invoke() {
    //hostName為登錄檔上項的名稱
    var hostName = "com.google.chrome.demo";
    //啟動本地應用程式
    var port = chrome.runtime.connectNative(hostName);
    updateResult("result1", "invoke..");
}

//為button1新增監聽事件
document.addEventListener('DOMContentLoaded', function () {
    document.querySelector('#button1').addEventListener(
        'click', invoke);
});