
const vm = new Vue({
    el: "#app",
    data: function () {
        return {
            msg: "Hello vue !",
            inputData: "test",
            items: []
        };
    },
    methods: {
        addItem: function () {
            var item = this.inputData;
            this.items.push(item);
        },
        loadData: function () {
            var vm = this;
            chrome.storage.local.get("tempData", function (result) {
                vm.items = result.tempData;
            });
        },
        saveData: function () {
            chrome.storage.local.set({ tempData: this.items }, function () {
                console.log("Value is set");
            });
        },
        deleteData: function (index) {
            this.items.splice(index, 1);
        }
    }
})